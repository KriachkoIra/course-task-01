const express = require("express");
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.text());

const PORT = process.env.PORT ?? 56201;

app.post("/square", function(req, res){
    const num = Number(req.body);
    const result = num*num;
    res.send({
        number: num,
        square: result,
    });
});

app.post("/reverse", function(req, res){
    const text = req.body;
    const result = text.split('').reverse().join('');
    res.send(result);

});

app.get('/date/:year/:month/:day', function(req, res) {
    const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const year = Number(req.params.year);
    const month = Number(req.params.month) - 1;
    const day = Number(req.params.day);

    const date = new Date(year, month, day);
    const now = new Date();

    const leapYear = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);

    const difference = Math.abs(Math.ceil((date - now) / (1000*60*60*24)));

    res.send({
        weekDay: daysOfWeek[date.getDay()],
        isLeapYear: leapYear,
        difference: difference,
    });
});


app.listen(PORT, function(){
    console.log('App listening on port ${PORT}.');
});